var Sys = {

	'contactsType':['E-mail','Telefone'],
	'fieldsRequered':['Nome','CEP'],
	'fieldsTable':['Nome','Sobrenome','Telefone','CEP','Criado_Em','Alterado_Em'],

	'db':null,
	'actIsEdit':false,
	'tmpUnidPerson':null,
	'emptyPerson':null,
	'isFilter':false,

	'start':function(){
		//Carregamento
		Sys.emptyPerson = Sys.getEmptyPerson();
		Sys.listPerson();

		//Acoes para Botoes
		$('#add').click(function(){
			$('.modal-body').html(Sys.emptyPerson);
			$('.modal-title').html('Novo Cliente');
			$('#modal').modal();
			Sys.actIsEdit=false;console.log('abrindo')
		});

		$('#search').keyup(function(){
			Sys.filter($(this).val());
		});

		$('#salve').click(function(){
			var valida = Sys.validate();
			if(valida){
				if(!Sys.actIsEdit){
					Sys.addPerson();
				}else{
					Sys.editPerson(Sys.tmpUnidPerson);
				}
				$('#modal').modal('hide');
			}

			$('#search').val('')
		
		});

		$('#modal').on('show.bs.modal', function (e) {
		  	$('.CEP').mask('99999-999');
		  	$('.CPF').mask('999.999.999-99');
		  	$('.RG').mask('99;999;999-9');
		  	$('.Telefone').mask('(99) 9999-9999');
			$('.Celular').mask('(99) 9999-99999');

			$('.CEP').keyup(function(){
				Sys.loadCEP();
			});
		})
	},
	'loadBtns':function(){

		$('.btnremove').click(function(){
			var unid = $(this).attr('unid');
			Sys.removePerson(unid);
		});

		$('.btnedit').click(function(){
			var unid = $(this).attr('unid');
			$('.modal-body').html(Sys.emptyPerson);
			$('#modal').modal();
			var cliente = Sys.loadPerson(unid);
			$('.modal-title').html('Editar '+cliente);			
		});

		if(!Sys.isFilter){
			$('#search').val('')
		}
	},
	'loadPerson':function(unid){
		for(i in Sys.db.list){
			if(Sys.db.list[i]['unid']==unid){
				var Pessoa = Sys.db.list[i];
			}
		}

		$('.register').each(function(){
			var field = $(this).attr('name');
			var cat = $(this).attr('cat');
			$(this).val(Pessoa[cat][field])
		});
		Sys.actIsEdit=true;
		Sys.tmpUnidPerson=Pessoa['unid'];
		return Pessoa['name']['Nome']+' '+Pessoa['name']['Sobrenome'];
	},
	'loadCEP':function(){
		var cep = $('.CEP').val();
			cep = cep.replace('_','');
		if(cep.length==9){
			var api = 'http://api.postmon.com.br/v1/cep/'+cep;
			$.ajax({
			  url: api
			}).done(function(result) {
			  $('.UF').val(result.estado)
			  $('.Cidade').val(result.cidade)
			  $('.Endereco').val(result.logradouro)
			  $('.Bairro').val(result.bairro)
			  $('.Numero').focus();
			});
		}
	},
	'editPerson':function(unid){
		for(i in Sys.db.list){
			if(Sys.db.list[i]['unid']==unid){
				$('.register').each(function(){
					var field = $(this).attr('name');
					var cat = $(this).attr('cat');
					Sys.db.list[i][cat][field]=$(this).val();
					Sys.db.list[i]['meta']['Alterado_Em']= Sys.getMark();
				});
				;
			}
		}
		
		Sys.listPerson();
	},
	'removePerson':function(unid){
		for(i in Sys.db.list){
			if(Sys.db.list[i]['unid']==unid){
				delete Sys.db.list[i];
			}
		}
		Sys.listPerson();
	},
	'addPerson':function(){
		
		var field;
		var cat;
		var subcat;
		var Pessoa = Sys.templatePerson();
			Pessoa.unid = Sys.getMark(true)
			Pessoa.meta.Criado_Em = Sys.getMark();

		$('.register').each(function(){
			field = $(this).attr('name');
			cat = $(this).attr('cat');
			Pessoa[cat][field]=$(this).val();
		});

		if(!Sys.db){
			var database = new Object();
				database.list = new Array();
				database.list.push(Pessoa);
			Sys.db = database
		}else{
			Sys.db.list.push(Pessoa);
		}
		Sys.listPerson();
	},
	'filter':function(string){
		if(string!=''){
			var exp;
			var data;
			var recordset = new Array();

			for(i in Sys.db.list){
				for(e in Sys.db.list[i]['name']){
					exp = new RegExp(string+'.*');
					data = Sys.db.list[i]['name'][e];
					if(data.match(exp)){
						recordset.push(Sys.db.list[i])
					}
				}
			}
			Sys.isFilter = true;
			Sys.listPerson(recordset);
		}else{
			Sys.isFilter = false;
			Sys.listPerson(undefined);
		}
	},
	'listPerson':function(recordset){
		var html = '<div class="panel panel-default">';
  			html += '<div class="panel-heading">Meus Clientes</div>';
			html += '<table class="table">';
  		var body ='';
		var header=[];
		var source = [];
		var total = 0;

		if(!Sys.db){
			body +='<tr>';
			body +='<td colspan="'+(Sys.fieldsTable.length+1)+'">Nenhum Registro</td>';
			body +='</tr>';
		}else{

			source = (typeof(recordset)=='object')?recordset:Sys.db.list;
			for(i in source){
				total++;
				var header = [];
				body +='<tr>';
				for(e in source[i]){
					for(k in source[i][e]){
						if(Sys.fieldsTable.indexOf(k)> -1 && k!='TipoContato'){
							body +='<td>'+source[i][e][k]+'</td>';
							header.push(k)
						}
					}
				}
				body +='<td>';
				body += '<button type="button" unid="'+source[i]['unid']+'" class="btn btn-default btnedit" aria-label="Left Align">';
  				body += '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>';
				body += '</button> ';
				body += '<button type="button" unid="'+source[i]['unid']+'" class="btn btn-default btnremove" aria-label="Left Align">';
  				body += '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>';
				body += '</button> ';
				body +='</td>';
				body +='</tr>';
			}
		}
		html += '<thead>';
		header = (header.length>0)?header:Sys.fieldsTable;
		for(i in header){
			html += '<td>'+header[i].replace('_',' ')+'</td>';
		}
		
		html += (total>0)?'<td>Gerenciar</td>':'<td></td>';
		html += '</thead>';
		html += '<tbody>';
		html += (total>0)?body:'<tr><td>Nenhum registro</td></tr>';
		html += '</tbody>';
		html += '<tfooter>';
		html += '</tfooter>';
		html += '<tr><td Align="right" colspan="'+(Sys.fieldsTable.length+1)+'">'+total+' registro(s)</td></tr>';
		html += '</table>';
		html += '</div>';
		html += '</div>';
		$('#list').html(html);
		Sys.loadBtns();
	},
	'validate':function(){
		var status = true;
		var field;
		$('.form-control').each(function(){
			field = $(this).attr('name');
			if(Sys.fieldsRequered.indexOf(field)>-1&&$(this).val()==''){
				bootbox.alert('Por favor, preencha o campo '+field, function() {
	                $(this).focus();
				});
	            
	            status = false;
	            return false;
			}
		});
		return status;
	},
	'templatePerson':function(){
		//Estruturando Pessoa
		var Person = new Object();
			Person.name = new Object();
			Person.name.Nome='';
			Person.name.Sobrenome='';

			Person.code = new Object();
			Person.code.CPF='';
			//Person.code.RG='';

			Person.address = new Object();
			Person.address.CEP='';
			//Person.address.Endereco='';
			//Person.address.Numero='';
			//Person.address.Complemento='';
			//Person.address.Bairro='';
			Person.address.UF='';
			Person.address.Cidade='';

			Person.contacts = new Object();
			Person.contacts.TipoContato = Sys.contactsType;

			//Person.extra = new Object();
			//Person.extra.Nascido_Em = '';

			Person.meta = new Object();
			Person.meta.Criado_Em = '';
			Person.meta.Alterado_Em = '';

		return Person;	 
	},
	'getEmptyPerson':function(){
		var template = Sys.templatePerson();
		var html ='<div class="input-group">';
		var label;
		var foco = '';
		var first = true;
		for(i in template){
			foco = (first)?'autofocus="true"':'';
			if(i !='meta'){
				for(k in template[i]){
					if(k != 'TipoContato'){
						label = k.replace('_',' ');
						html +='<input cat="'+i+'" placeholder="'+label+'" class="form-control register '+k+'" type="text"name="'+k+'" '+foco+'/>';
					}else{
						for(l in template[i][k]){
							label = template[i][k][l].replace('_',' ');
							html +='<input cat="'+i+'" class="form-control register '+template[i][k][l]+'" placeholder="'+label+'" type="text"name="'+template[i][k][l]+'" '+foco+'/>';
						}
					}
				}
			}
			first = false;
		}
		html += '</div>';
		return html;
	},
	'getMark':function(unid){
	    var data = new Date();
	    var dia = data.getDate();
	    if (dia.toString().length == 1)
	      	dia = "0"+dia;
	    var mes = data.getMonth()+1;
	    if (mes.toString().length == 1)
	      	mes = "0"+mes;
	    var ano = data.getFullYear(); 
	    var hora = data.getHours();
	    if (hora.toString().length == 1)
	      	hora = "0"+hora;
	    var min = data.getMinutes();
	    if (min.toString().length == 1)
	      	min = "0"+min;
	    var sec = data.getSeconds(); 
	    if (sec.toString().length == 1)
	      	sec = "0"+sec;
	    var mil = data.getMilliseconds(); 

	    if(unid){
	    	res = ano+''+mes+''+dia+''+hora+''+min+''+sec+''+mil;
	    }else{
	    	res = dia+'/'+mes+'/'+ano+' '+hora+':'+min+':'+sec;
	    }
	    return res;
	}
}

$(document).ready(function(){
	Sys.start();
});